import { observable } from 'mobx';

export class TimeSlot {

  @observable name: string;
  @observable phone: string;

}
