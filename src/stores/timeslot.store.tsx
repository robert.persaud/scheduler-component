import { observable, computed, action, autorun} from 'mobx';
import { TimeSlot } from '../models/timeslot';

interface Hour {
  time: number;
  meridiem: string;
};

class TimeslotStore {

  @observable timeSlots: TimeSlot[] = [];

  @action update(timeslot: TimeSlot, el: Hour) {
    let slot = el.time + el.meridiem;
    const pos = this.findPosition(slot);
    this.timeSlots[pos] = timeslot;
  }

  @computed get emptyTimeSlots() {
    return this.timeSlots.filter(item => !item.name && !item.phone).length;
  }

  constructor() {
    autorun(() => {
      console.log('Number of openings => ', this.emptyTimeSlots);
    });

    // hacky - seeding in multiple places
    for (let i = 0; i < 9; i++) {
      this.timeSlots.push({name: '', phone: ''});
    }
  }

  // hacky
  findPosition(el) {
    switch(el) {
      case '9am': return 0;
      case '10am': return 1;
      case '11am': return 2;
      case '12pm': return 3;
      case '1pm': return 4;
      case '2pm': return 5;
      case '3pm': return 6;
      case '4pm': return 7;
      case '5pm': return 8;
    }
  }

}

export default new TimeslotStore();