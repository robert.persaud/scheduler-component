import { Component, Prop, Event, EventEmitter } from '@stencil/core';
import { computed } from 'mobx';

@Component({
  tag: 'hourly-component',
  styleUrl: 'hourly-component.css'
})
export class HourlyComponent {

  @Prop() time: number;
  @Prop() meridiem: string = 'am';
  @Prop() store: any; // quiz - what IS the type? TimeslotStore?
  @Event() itemSelected: EventEmitter;
  @Event() itemUpdated: EventEmitter;

  itemSelectedHandler(ev) {
    this.itemSelected.emit(ev);
  }

  @computed get isBooked() {
    return this.store.name !== '' || this.store.phone !== '';
  }

  // @todo breaks computed
  // componentDidLoad() {
  //   autorun(() => {
  //       console.log(this.time + this.meridiem + ' booked?', this.isScheduled);
  //   })
  // }

  render() {

    return (
      <div class="columns is-mobile has-text-centered">
        <div class="column is-one-third vertical-center">
          <a class={"link is-info " + (this.isBooked ? 'is-size-5' : 'is-size-7')}>{this.store && this.store.name || 'empty'}</a>
        </div>
        <div class="column is-one-third selector">
        <a href="#" class={this.isBooked ? 'menu-red' : 'menu'} onClick={() => this.itemSelectedHandler({time: this.time, meridiem: this.meridiem})}>{this.time}{this.meridiem}</a>
        </div>
        <div class="column is-one-third vertical-center">
          <a class={"link is-info " + (this.isBooked ? 'is-size-5' : 'is-size-7')}>{this.store && this.store.phone || 'empty'}</a>
        </div>
      </div>

    );
  }
}
