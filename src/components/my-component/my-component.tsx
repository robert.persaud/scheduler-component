import { Component, State, Listen } from '@stencil/core';
import TimeslotStore from '../../stores/timeslot.store';
import { TimeSlot } from '../../models/timeslot';

const PERIOD = {
  ANTE: 'am', // AM, Am
  POST: 'pm'
};

const SEED_DATA = [
  { time: 9, meridiem: PERIOD.ANTE },
  { time: 10, meridiem: PERIOD.ANTE },
  { time: 11, meridiem: PERIOD.ANTE },
  { time: 12, meridiem: PERIOD.POST },
  { time: 1, meridiem: PERIOD.POST },
  { time: 2, meridiem: PERIOD.POST },
  { time: 3, meridiem: PERIOD.POST },
  { time: 4, meridiem: PERIOD.POST },
  { time: 5, meridiem: PERIOD.POST }
];

interface Hour {
  time: number;
  meridiem: string;
};

@Component({
  tag: 'my-component',
  styleUrl: 'my-component.css',
  shadow: false // use custom css to externally style shadow dom
})
export class MyComponent {

  @State() active: boolean = false;
  @State() currentHour: Hour;
  @State() selectedItem: TimeSlot;

  @Listen('itemSelected')
  itemSelectedHandler(event: CustomEvent) {
    this.currentHour = event.detail;
    const pos = TimeslotStore.findPosition(this.currentHour.time + this.currentHour.meridiem);
    this.selectedItem = TimeslotStore.timeSlots[pos];
    this.active = !this.active;
  }

  @Listen('toggleModal')
  dismissModal(event: CustomEvent) {
    this.active = event.detail;
  }

  @Listen('savingChanges')
  saveHandler(event: CustomEvent) {
    TimeslotStore.update(event.detail, this.currentHour);
  }

  getCurrentItem() {
    const pos = TimeslotStore.findPosition(this.currentHour.time + this.currentHour.meridiem);
    this.selectedItem = TimeslotStore.timeSlots[pos];
  }

  render() {

    var list = SEED_DATA.map((slot, index) =>
      <li><hourly-component store={TimeslotStore.timeSlots[index]} time={slot.time} meridiem={slot.meridiem}></hourly-component></li>
    );

    let [date] = new Date().toLocaleString('en-US').split(', ');

    return ([
      <section class="section">
        <div class="container">
          <h2 class="title is-2 has-text-centered">{ date }</h2>
          <ul class="list">
            { list }
          </ul>
        </div>
      </section>,
      <modal-component isActive={this.active} content={this.selectedItem}></modal-component>
    ]);
  }
}
