import { Component, Prop, Element, Event, EventEmitter } from '@stencil/core';
import { TimeSlot } from '../../models/timeslot';

@Component({
  tag: 'modal-component',
  styleUrl: 'modal-component.css'
})
export class ModalComponent {

  @Prop() isActive: boolean = false;
  @Prop() content: TimeSlot;

  @Event() toggleModal: EventEmitter;
  @Event() savingChanges: EventEmitter;

  @Element() el: HTMLElement;

  dismiss() {
    this.toggleModal.emit(false);
  }

  nameChanged(event) {
    this.content.name = event.target.value;
  }

  phoneChanged(event) {
    this.content.phone = event.target.value;
  }

  saveChanges() {

      var name = this.el.querySelector('.input.name')['value'];
      var phone = this.el.querySelector('.input.phone')['value'];

      // debouncer
      setTimeout(() => {
        this.savingChanges.emit({ name: name, phone: phone });
        this.dismiss();
      }, 100);

  }

  isEmpty() {
    return Object.keys(this.content || []).map(key => this.content[key]).filter(i => i).length === 0;
  }

  render() {

    console.log('#content =>', this.content);

    return (
      <div class={'modal ' + (this.isActive ? 'is-active' : null)}>
        <div class="modal-background"></div>
        <div class="modal-card">
          <header class="modal-card-head">
            <p class="modal-card-title">Schedule Appointment</p>
            <button class="delete" aria-label="close" onClick={() => this.dismiss()}></button>
            </header>
          <section class="modal-card-body">
            <div class="field">
              <label class="label">Name</label>
              <div class="control">
                <input class="input name" type="text" value={ this.content && this.content.name } placeholder="Text input"
                  onChange={(event: UIEvent) => this.nameChanged(event)}/>
              </div>
            </div>
            <div class="field">
              <label class="label">Phone</label>
              <div class="control">
                <input class="input phone" type="text" placeholder="Text input" value={ this.content && this.content.phone }
                onChange={(event: UIEvent) => this.phoneChanged(event)} />
              </div>
            </div>
          </section>
          <footer class="modal-card-foot">
            <button class="button is-success" onClick={() => this.saveChanges()}>Save changes</button>
            <button class="button" onClick={() => this.dismiss()}>Cancel</button>
            </footer>
          </div>
      </div>
    );
  }
}
